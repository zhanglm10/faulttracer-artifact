/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.rts;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import set.faulttracer.coverage.io.TracerIO;
import set.faulttracer.diff.VersionDiff;
import set.faulttracer.utils.Properties;

/**
 * Selecting affected test classes via method-level regression test selection
 * (e.g., FaultTracer and Chianti)
 * 
 * @author lingmingzhang
 *
 */
@Deprecated
public class TC_MethLevelRTS
{
	private static Logger logger = Logger.getLogger(TC_MethLevelRTS.class);

	public static void main(String[] args) throws Exception {
		for (int level = 0; level <= 10; level++) {
			long startTime = System.currentTimeMillis();
			// compute version diff
			//VersionDiff.main(args);

			// obtain old cov info
			Map<String, Set<String>> old_cov_map = TracerIO
					.loadCovFromDirectory(args[0]);

			Map<String, Set<String>> new_cov_map = TracerIO
					.loadCovFromDirectory(args[1]);
			Set<String> selTests = new HashSet<String>();

			// if no file changes, directly return
			for (String test : new_cov_map.keySet()) {
				// select new tests
				if (!old_cov_map.containsKey(test))
					selTests.add(test);
				// select changed tests
				else if (VersionDiff.changedFiles
						.contains(test.replace(".", "/"))) {
					selTests.add(test);
				}
				// select impacted tests
				else if (isAffected(old_cov_map.get(test), level))
					selTests.add(test);
			}
			logger.info("===========selected tests===========");
			for (String test : selTests)
				logger.info(test);

			long endTime = System.currentTimeMillis();
			System.out.print(
					selTests.size() + "|" + (endTime - startTime) + "ms ");
		}
		System.out.println();
	}

	public static boolean isAffected(Set<String> ecg, int level) {
		Set<String> classCov = TC_ClassLevelRTS.preprocessClass(ecg);
		logger.debug("Class dependency: " + classCov);

		// handle class deletions
		for (String c : VersionDiff.deletedFiles) {
			if (classCov.contains(c))
				return true;
		}

		if (Properties.CLASS_COV.equals(Properties.TRACER_COV_TYPE)) {
			// handle class changes
			for (String c : VersionDiff.changedFiles) {
				if (classCov.contains(c))
					return true;
			}
			return false;
		}

		// handle class-head changes
		for (String c : VersionDiff.classHeaderChanges) {
			if (classCov.contains(c))
				return true;
		}

		// check if the transformed class changes are covered
		for (String c : VersionDiff.transformedClassChanges) {
			if (classCov.contains(c))
				return true;
		}

		// handle static initializer changes
		for (String cm : VersionDiff.CSIs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle static initializer deletions
		for (String cm : VersionDiff.DSIs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle initializer changes
		for (String cm : VersionDiff.CIs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle initializer deletions
		for (String cm : VersionDiff.DIs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle method changes
		for (String cm : VersionDiff.CSMs) {
			if (ecg.contains(cm))
				return true;
		}
		for (String cm : VersionDiff.CIMs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle method deletions
		for (String cm : VersionDiff.DSMs) {
			if (ecg.contains(cm))
				return true;
		}
		for (String cm : VersionDiff.DIMs) {
			if (ecg.contains(cm))
				return true;
		}
		// handle method lookup changes
		for (String cm : VersionDiff.LCs) {
			if (ecg.contains(cm))
				return true;
		}
		return false;
	}

}
