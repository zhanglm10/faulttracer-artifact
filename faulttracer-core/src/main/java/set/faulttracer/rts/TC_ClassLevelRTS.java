/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.rts;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import set.faulttracer.coverage.io.TracerIO;
import set.faulttracer.diff.VersionDiff;

/**
 * Selecting affected test classes via class-level regression test selection
 * (e.g., FaultTracer and Chianti)
 * 
 * @author lingmingzhang
 *
 */
@Deprecated
public class TC_ClassLevelRTS
{
	private static Logger logger = Logger.getLogger(TC_ClassLevelRTS.class);

	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();
		//VersionDiff.main(args);
		logger.debug("Changed files: " + VersionDiff.changedFiles);
		logger.debug("Deleted files: " + VersionDiff.deletedFiles);
		Map<String, Set<String>> old_cov_map = TracerIO
				.loadCovFromDirectory(args[2]);
		Map<String, Set<String>> new_cov_map = TracerIO.loadCovFromDirectory(args[3]);

		for (String test : old_cov_map.keySet())
			logger.debug("Method coverage for " + test + ": "
					+ old_cov_map.get(test));
		Set<String> selTests = new HashSet<String>();
		for (String test : new_cov_map.keySet()) {
			// select new tests
			if (!old_cov_map.containsKey(test))
				selTests.add(test);
			// select changed tests
			else if (VersionDiff.changedFiles
					.contains(test.replace(".", "/"))) {
				selTests.add(test);
			}
			// select impacted tests
			else if (isAffected(old_cov_map.get(test)))
				selTests.add(test);
		}
		logger.info("===========selected tests===========");
		for (String test : selTests)
			logger.info(test);
		long endTime = System.currentTimeMillis();
		System.out.println();
		System.out.println("Class-level SelTests: "+ selTests
				.size()
						  + " Time: " + (endTime - startTime)  + "ms"
						 );
	}

	public static boolean isAffected(Set<String> set) {
		Set<String> classCov = preprocessClass(set);
		logger.debug("Class dependency: " + classCov);
		// handle class deletions
		for (String c : VersionDiff.deletedFiles) {
			if (classCov.contains(c))
				return true;
		}
		// handle class changes
		for (String c : VersionDiff.changedFiles) {
			if (classCov.contains(c))
				return true;
		}

		return false;
	}

	public static Set<String> preprocessClass(Set<String> content) {
		Set<String> res = new HashSet<String>();
		for (String item : content) {
			res.add(item.substring(0, item.indexOf(":")));
		}
		return res;
	}

	static String removeFlag(String s) {
		if (!s.contains(" "))
			return s;
		return s.substring(0, s.indexOf(" "));
	}
}
