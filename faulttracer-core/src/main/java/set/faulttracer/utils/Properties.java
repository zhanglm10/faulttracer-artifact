/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.utils;

import java.io.PrintStream;

import set.faulttracer.coverage.core.CoverageData;
import set.faulttracer.diff.VersionDiff;

public class Properties
{
	public static final String CLASS = "set/faulttracer/utils/Properties";
	public static final String TRACER_ROOT_DIR = "faulttracer-files";
	public static final String GZ_EXT = ".gz";
	public static final String FTRACER_TAG = "[FaultTracer] ";

	public static String TRACER_CLASS = "set/faulttracer/coverage/io/Tracer";
	public static String FILE_CHECKSUM = null;

	public static String PREFIX_KEY = "prefix=";
	public static String PROJECT_PREFIX = null;

	public static String COV_TYPE_KEY = "covLevel=";
	public static final String STMT_COV = "stmt-cov";
	public static final String BLK_COV = "block-cov";
	public static final String BRANCH_COV = "branch-cov";
	public static final String CLASS_COV = "class-cov";
	public static final String METH_COV = "meth-cov";
	// can also be "class-cov","meth-cov", "branch-cov", "block-cov",
	// "stmt-cov", or null
	public static String TRACER_COV_TYPE = null;

	public static String OLD_DIR_KEY = "oldVersion=";
	// by default treating the current dir as old dir as well
	public static String OLD_DIR = ".";
	
	public static String NEW_DIR_KEY = "newVersion=";
	// by default treating the current dir as old dir as well
	public static String NEW_DIR = ".";

	public static String NEW_CLASSPATH_KEY = "bytecodeLocation=";
	// by default search "target" dir for bytecode files
	public static String NEW_CLASSPATH = "target";

	public static String LEVEL_KEY = "hybridConfig=";

	public static String EXECUTION_ONLY_KEY = "execOnly=";
	// by default collect new coverage for each test
	public static boolean EXECUTION_ONLY = false;

	public static String RTS_KEY = "RTS=";
	// by default always perform RTS
	public static boolean RTS = true;

	public static String TEST_LEVEL_KEY = "testLevel=";
	public final static String TM_LEVEL = "test-meth";
	public final static String TC_LEVEL = "test-class";
	// by default collect test-class level information
	public static String TEST_LEVEL = TC_LEVEL;

	// by default do not trace lib coverage, but will when the current project
	// prefix is not assigned
	public static boolean SCOPE_WITH_LIB = false;

	public static void processAgentArguments(String arguments) {
		PrintStream printer = System.out;
		if (arguments == null)
			return;
		String[] items = arguments.split(",");
		String hybridCode = "0";
		for (String item : items) {
			if (item.startsWith(PREFIX_KEY)) {
				PROJECT_PREFIX = item.replace(PREFIX_KEY, "").replace('.', '/');
			} else if (item.startsWith(RTS_KEY)) {
				RTS = Boolean.parseBoolean(item.replace(RTS_KEY, ""));
			} else if (item.startsWith(COV_TYPE_KEY)) {
				TRACER_COV_TYPE = item.replace(COV_TYPE_KEY, "");
			} else if (item.startsWith(TEST_LEVEL_KEY)) {
				TEST_LEVEL = item.replace(TEST_LEVEL_KEY, "");
			} else if (item.startsWith(OLD_DIR_KEY)) {
				OLD_DIR = item.replace(OLD_DIR_KEY, "");
			}  else if (item.startsWith(NEW_DIR_KEY)) {
				NEW_DIR = item.replace(NEW_DIR_KEY, "");
			} else if (item.startsWith(NEW_CLASSPATH_KEY)) {
				NEW_CLASSPATH = item.replace(NEW_CLASSPATH_KEY, "");
			} else if (item.startsWith(EXECUTION_ONLY_KEY)) {
				EXECUTION_ONLY = Boolean
						.parseBoolean(item.replace(EXECUTION_ONLY_KEY, ""));
			} else if (item.startsWith(LEVEL_KEY)) {
				String hybridInfo = item.replace(LEVEL_KEY, "");
				for (int i = 0; i < hybridInfo.length(); i++) {
					char tag = hybridInfo.charAt(i);
					if (tag > 'z' || tag < 'a')
						continue;
					switch (tag) {
					case 'a':
						VersionDiff.transDSI = true;
						break;
					case 'b':
						VersionDiff.transASI = true;
						break;
					case 'c':
						VersionDiff.transCSI = true;
						break;
					case 'd':
						VersionDiff.transDI = true;
						break;
					case 'e':
						VersionDiff.transAI = true;
						break;
					case 'f':
						VersionDiff.transCI = true;
						break;
					case 'g':
						VersionDiff.transDSM = true;
						break;
					case 'h':
						VersionDiff.transASM = true;
						break;
					case 'i':
						VersionDiff.transCSM = true;
						break;
					case 'j':
						VersionDiff.transDIM = true;
						break;
					case 'k':
						VersionDiff.transAIM = true;
						break;
					case 'l':
						VersionDiff.transCIM = true;
						break;
					}
				}
			}
		}

		// coverage type should always be set
		if (TRACER_COV_TYPE == null) {
			System.out.println(FTRACER_TAG + "No RTS for this run: property \""
					+ COV_TYPE_KEY.substring(0, COV_TYPE_KEY.length() - 1)
					+ "\" has to be set");
			System.out.println(FTRACER_TAG
					+ "Usage: JavaAgent parameters: \n----" + PREFIX_KEY
					+ "prefix of project under test (e.g., \"org.joda.time\"),\n----"
					+ COV_TYPE_KEY
					+ "RTS coverage level (e.g., \"meth-cov\", and \"class-cov\"),\n----"
					+ LEVEL_KEY
					+ "RTS hybrid levels for method level (e.g., \"1\", and \"123\", default no hybrid with class level),\n----"
					+ OLD_DIR_KEY
					+ "old version path (e.g., \".\", default current dir),\n----"
					+ NEW_CLASSPATH_KEY
					+ "path to bytecode files for version diff (e.g., \"target\", default \"target\" dir),\n----"
					+ EXECUTION_ONLY_KEY
					+ "execute current tests without coverage or not (e.g., \"true\" and \"false\", default \"false\")");
			return;
		}

		// if prefix is not set, trace coverage for all libs
		if (PROJECT_PREFIX == null) {
			SCOPE_WITH_LIB = true;
		}
		// does not support RTS for test method level or for branch/stmt
		// coverage
		if (TEST_LEVEL.equals(TM_LEVEL) || BRANCH_COV.equals(TRACER_COV_TYPE)
				|| STMT_COV.equals(TRACER_COV_TYPE)) {
			RTS = false;
			// do not need to trace runtime info if no RTS
			CoverageData.WITH_RTINFO = false;
		}
		// create dir for storing checksum for RTS
		if (RTS) {
			FILE_CHECKSUM = TRACER_COV_TYPE.substring(0,
					TRACER_COV_TYPE.indexOf("-")) + "-checksum";
		} else {
			CoverageData.WITH_RTINFO = false;
		}
		// when both AIM and DIM changes are transformed into class
		// changes, no need to trace runtime type info
		if (VersionDiff.transAIM && VersionDiff.transDIM) {
			hybridCode = "13";
			CoverageData.WITH_RTINFO = false;
			if (RTS && Properties.METH_COV.equals(Properties.TRACER_COV_TYPE)) {
				Properties.TRACER_COV_TYPE += "-hybrid";
				FILE_CHECKSUM += "-hybrid";
			}
		} else if (!hybridCode.equals("0")) {
			int offset = (hybridCode.charAt(0) - 'a') + 1;
			hybridCode = offset + "";
		}

		printer.println(
				Properties.FTRACER_TAG + "Coverage level: " + TRACER_COV_TYPE);
		printer.println(Properties.FTRACER_TAG
				+ "With RunTime Info?: "
				+ CoverageData.WITH_RTINFO);
		printer.println(Properties.FTRACER_TAG + "Test level: " + TEST_LEVEL);
		printer.println(
				Properties.FTRACER_TAG + "Project prefix: " + PROJECT_PREFIX);

		// added for experiments, default class level code (i.e., 10)
		printer.println(FTRACER_TAG + "Hybrid code: " + hybridCode);
		printer.println(Properties.FTRACER_TAG + "Hybrid config: "
				+ VersionDiff.transDSI + "|" + VersionDiff.transASI + "|"
				+ VersionDiff.transCSI + "|" + VersionDiff.transDI + "|"
				+ VersionDiff.transAI + "|" + VersionDiff.transCI + "|"
				+ VersionDiff.transDSM + "|" + VersionDiff.transASM + "|"
				+ VersionDiff.transCSM + "|" + VersionDiff.transDIM + "|"
				+ VersionDiff.transAIM + "|" + VersionDiff.transCIM + "|");
		printer.println(
				Properties.FTRACER_TAG + "Old version location: " + OLD_DIR);
		printer.println(Properties.FTRACER_TAG
				+ "Bytecode location to parse checksum: " + NEW_CLASSPATH);
		printer.println(Properties.FTRACER_TAG + "Execution only config: "
				+ EXECUTION_ONLY);

	}

}
