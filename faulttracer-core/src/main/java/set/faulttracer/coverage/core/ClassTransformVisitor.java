/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.core;

import java.util.BitSet;
import java.util.HashSet;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.faulttracer.utils.Properties;

public class ClassTransformVisitor extends ClassVisitor implements Opcodes
{
	int clazzId;
	String clazzName;

	public ClassTransformVisitor(int clazzId, String clazzName,
			final ClassVisitor cv) {
		super(ASM5, cv);
		this.clazzId = clazzId;
		this.clazzName = clazzName;
	}

	@Override
	public MethodVisitor visitMethod(final int access, final String name,
			final String desc, final String signature,
			final String[] exceptions) {
		String methName = name + ":" + desc;
		int methId = CoverageData.registerMeth(clazzId, methName);
		boolean isInit = name.startsWith("<init>");
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
				exceptions);
		if (Properties.STMT_COV.equals(Properties.TRACER_COV_TYPE))
			return mv == null ? null
					: new MethodVisitorStmtCov(mv, clazzId, methId, clazzName,
							methName, isInit, access);
		if (Properties.BRANCH_COV.equals(Properties.TRACER_COV_TYPE))
			return mv == null ? null
					: new MethodVisitorBranchCov(mv, clazzId, methId, clazzName,
							methName, isInit, access);
		if (Properties.TRACER_COV_TYPE.startsWith(Properties.METH_COV))
			return mv == null ? null
					: new MethodVisitorMethCov(mv, clazzId, methId, clazzName,
							methName, isInit, access);
		if (Properties.CLASS_COV.equals(Properties.TRACER_COV_TYPE))
			return mv == null ? null
					: new MethodVisitorClassCov(mv, clazzId, methId, clazzName,
							methName, isInit, access);
		if (Properties.BLK_COV.equals(Properties.TRACER_COV_TYPE))
			return mv == null ? null
					: new MethodVisitorBlockCov(mv, clazzId, methId, clazzName,
							methName, isInit, access);
		return mv;
	}

	@Override
	public void visitEnd() {
		super.visitEnd();
		if (Properties.TRACER_COV_TYPE.startsWith(Properties.METH_COV)) {
			// bug fix: add check for the case that no method has been
			// registered
			if (CoverageData.idMethMap.containsKey(clazzId)) {
				CoverageData.methCovArray[clazzId] = new boolean[CoverageData.idMethMap
						.get(clazzId).size()];
			}
			// if (CoverageData.WITH_RTINFO)
			// CoverageData.rtType.put(clazzName, new HashSet<String>());
		}
		if (Properties.TRACER_COV_TYPE.equals(Properties.BLK_COV)) {
			if (CoverageData.idMethMap.containsKey(clazzId)) {
				CoverageData.methCovArray[clazzId] = new boolean[CoverageData.idMethMap
						.get(clazzId).size()];
				CoverageData.blockCovSet[clazzId] = new BitSet[CoverageData.idMethMap
						.get(clazzId).size()];
				for(int i=0;i< CoverageData.blockCovSet[clazzId].length;i++)
					CoverageData.blockCovSet[clazzId][i]=new BitSet();
			}
		} else if (Properties.STMT_COV.equals(Properties.TRACER_COV_TYPE)) {
			CoverageData.stmtCovSet[clazzId] = new BitSet();
		}
	}

}
