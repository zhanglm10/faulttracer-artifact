/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.core;

import org.apache.log4j.Logger;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AnalyzerAdapter;
import org.objectweb.asm.tree.analysis.Analyzer;
import org.objectweb.asm.tree.analysis.AnalyzerException;

import set.faulttracer.rts.HybridRTSWithBlock;

class MethodVisitorBlockCov extends MethodVisitor implements Opcodes
{
	
	private static Logger logger = Logger.getLogger(MethodVisitorBlockCov.class);


	int clazzId;
	int methId;
	String clazzName;
	String methName;
	boolean isVirtual;
	boolean isInit;
	int labelId=0;

	public MethodVisitorBlockCov(final MethodVisitor mv, int clazzId, int methId,
			String clazzName, String methName, boolean isInit, int access) {
		super(ASM5, mv);
		this.clazzId = clazzId;
		this.methId = methId;
		this.clazzName = clazzName;
		this.methName = methName;
		this.isInit = isInit;
		//logger.info(clazzName+":"+methName);
	}
	
	@Override
	public void visitLabel(Label label){
		super.visitLabel(label);
		mv.visitLdcInsn(clazzId);
		mv.visitLdcInsn(methId);
		//logger.info(labelId);
		mv.visitLdcInsn(labelId++);

		mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER, Tracer.TRACE_BLOCK_COV,
				"(III)V", false);
	}
	
	// trace the method invocations
	@Override
	public void visitCode() {
		// method coverage
			mv.visitLdcInsn(clazzId);
			mv.visitLdcInsn(methId);
			mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
					Tracer.TRACE_METH_COV, "(II)V", false);
		
		// runtime type info
		if (CoverageData.WITH_RTINFO && isVirtual && !isInit) {
			mv.visitLdcInsn(clazzId);
			mv.visitLdcInsn(methId);
			mv.visitLdcInsn(clazzName);
			mv.visitLdcInsn(methName);
			
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Object", "getClass",
					"()Ljava/lang/Class;", false);
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Class", "getName",
					"()Ljava/lang/String;", false);
			mv.visitMethodInsn(INVOKESTATIC, CoverageData.TRACER,
					Tracer.TRACE_METH_COV_RT, "(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", false);
		}
		super.visitCode();
	}

	@Override
	public void visitFieldInsn(int opcode, String owner, String name,
			String desc) {
		mv.visitFieldInsn(opcode, owner, name, desc);
		if (/* opcode==Opcodes.PUTSTATIC|| */opcode == Opcodes.GETSTATIC) {
				mv.visitLdcInsn(owner);
				mv.visitMethodInsn(Opcodes.INVOKESTATIC, CoverageData.TRACER,
						Tracer.TRACE_CLINIT, "(Ljava/lang/String;)V", false);
		}
	}

	public void visitMaxs(int maxStack, int maxLocals) {
		mv.visitMaxs(maxStack + 4, maxLocals);
	}

	public boolean isVirtual(int access) {
		return 0 == (access & Opcodes.ACC_STATIC);
	}

}