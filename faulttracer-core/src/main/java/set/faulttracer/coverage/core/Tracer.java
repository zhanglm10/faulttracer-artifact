/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.core;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class Tracer
{
	public final static String TRACE_CLASS_COV = "traceClassCovInfo";
	public final static String TRACE_METH_COV = "traceMethCovInfo";
	public final static String TRACE_METH_COV_RT = "traceMethCovInfoWithRT";
	public final static String TRACE_CLINIT = "traceMethCovInfoClinit";
	public final static String TRACE_STMT_COV = "traceStmtCovInfo";
	public final static String TRACE_BLOCK_COV = "traceBlockCovInfo";
	public final static String TRACE_BRANCH_COV = "traceBranchCovInfo";

	// trace class coverage
	public static void traceClassCovInfo(int clazzId) {
		CoverageData.classCovArray[clazzId] = true;
	}

	public static void traceClassCovInfo(String clazz) {
		Integer clazzId = CoverageData.classIdMap.get(clazz);
		if (clazzId != null)
			CoverageData.classCovArray[clazzId] = true;
	}

	// trace method coverage
	public static void traceMethCovInfo(int clazzId, int methId) {
		CoverageData.methCovArray[clazzId][methId] = true;
	}

	public static void traceMethCovInfoWithRT(int clazzId, int methId,
			String clazzName, String methName, String rtype) {
		// System.out.println(">>"+clazzName+"|"+rtype);
		if (rtype.equals(clazzName))
			return;
		// if (CoverageData.rtType.containsKey(clazzName))
		Set<String> set = CoverageData.rtType.get(clazzName);
		if (set == null) {
			Set<String> newSet = new HashSet<String>();
			newSet.add(methName);
			CoverageData.rtType.put(clazzName, newSet);
		} else
			set.add(methName);
	}

	public static void traceMethCovInfoClinit(String clazz) {
		Integer clazzId = CoverageData.classIdMap.get(clazz);
		if (clazzId == null)
			return;
		CoverageData.methCovArray[clazzId][0] = true;
	}

	// trace statement coverage
	public static void traceStmtCovInfo(int clazzId, int line) {
		CoverageData.stmtCovSet[clazzId].set(line);
	}

	// basic block coverage
	public static void traceBlockCovInfo(int clazzId, int methId, int labelId){
		CoverageData.blockCovSet[clazzId][methId].set(labelId);
	}
	
	// trace branch coverage
	public static void traceBranchCovInfo(String branch, String id) {
		String key = id;
		ConcurrentMap<String, Integer> methodMap = CoverageData.branchCov;
		if (branch.length() > 0) {
			key = key + "<" + branch + ">";
			if (branch.equals("true")) {
				String resetKey = id + "<false>";
				methodMap.put(resetKey, methodMap.get(resetKey) - 1);
				if (methodMap.get(resetKey) == 0)
					methodMap.remove(resetKey);
			}
		}
		if (!methodMap.containsKey(key)) {
			methodMap.put(key, 1);
		} else
			methodMap.put(key, methodMap.get(key) + 1);
	}
}
