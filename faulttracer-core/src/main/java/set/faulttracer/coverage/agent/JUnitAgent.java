/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.agent;

import java.lang.instrument.Instrumentation;

import org.apache.log4j.Logger;

import set.faulttracer.coverage.junit.JUnitTestClassLevelTransformer;
import set.faulttracer.coverage.junit.JUnitTestMethodLevelTransformer;
import set.faulttracer.utils.Properties;

public class JUnitAgent
{
	private static Logger logger = Logger.getLogger(JUnitAgent.class);

	public static void premain(String args, Instrumentation inst)
			throws Exception {
		logger.debug("Agent arguments: " + args);
		Properties.processAgentArguments(args);
		if (Properties.TRACER_COV_TYPE != null) {
			if (!Properties.EXECUTION_ONLY)
				inst.addTransformer(new ClassTransformer());
			if (Properties.TM_LEVEL.equals(Properties.TEST_LEVEL))
				inst.addTransformer(new JUnitTestMethodLevelTransformer());
			else
				inst.addTransformer(new JUnitTestClassLevelTransformer());
		}
	}
}
