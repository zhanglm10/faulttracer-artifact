/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.coverage.junit;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import set.faulttracer.coverage.io.TracerIO;
import set.faulttracer.diff.ClassContentParser;
import set.faulttracer.rts.HybridRTS;
import set.faulttracer.rts.HybridRTSWithBlock;
import set.faulttracer.utils.Properties;

class JUnitTestRunStartEndEventMethodVisitor extends MethodVisitor
		implements Opcodes
{
	String mName;

	public JUnitTestRunStartEndEventMethodVisitor(final MethodVisitor mv,
			String name) {
		super(ASM5, mv);
		this.mName = name;
	}

	@Override
	public void visitCode() {
		if (mName.equals("fireTestRunStarted")) {
			// if RTS, then diff the two versions to compute affected tests
			if (Properties.RTS) {
				// if this is the test run start, check for test exclusion
				if (!Properties.TRACER_COV_TYPE.equals(Properties.BLK_COV))
					mv.visitMethodInsn(INVOKESTATIC, HybridRTS.CLASS, "main",
							"()V", false);
				else
					mv.visitMethodInsn(INVOKESTATIC, HybridRTSWithBlock.CLASS,
							"main", "()V", false);
			}
		} else {
			// if this is the test run end, clean obsolete test cov and check
			// for consistency
			mv.visitMethodInsn(INVOKESTATIC, TracerIO.CLASS,
					"cleanObsoleteTestCov", "()V", false);
		}
		mv.visitCode();
	}
}