library(ggplot2)
library(reshape2)
library(wesanderson)

subs<-c("invokebinder", "compile-testing", "logstash-logback-encoder", "commons-cli", "joda-time", "commons-dbutils", "commons-validator", "commons-fileupload", "asterisk-java", "commons-functor", "la4j", "commons-jxpath", "commons-email", "commons-compress", "commons-codec", "jfreechart", "commons-collections", "commons-lang", "commons-imaging", "commons-configuration", "commons-net", "closure-compiler", "java-apns", "commons-io", "commons-math", "commons-dbcp", "log4j", "stream-lib", "HikariCP", "OpenTripPlanner", "commons-pool", "mapdb")

techs<-c("HRTS", "CRTS")
paperDir<-"fse17-results/"
print_decimal <- function(x, k) format(round(x, k), nsmall=k)
sep<-"&"


sink("fse17-results/table5.tex");
cat("\\begin{table*}[t!]\n\\small\\center\n");
cat("\\begin{adjustbox}{width=0.9\\textwidth}");
cat("\\begin{tabular}{|l|r||rrr|rrr|rrr|rrr||r|}\n\\hline\n");
#cat("\\begin{tabular}{|l|l||lll|lll|lll|lll||l|}\n\\hline\n");
cat("&&\\multicolumn{12}{c|}{Transformed \\methlevel{} Changes}&\\\\ \\cline{3-14}\n");
cat("Subs&\\mrts&\\DSI&\\ASI&\\CSI&\\DI&\\AI&\\CI&\\DSM&\\ASM&\\CSM&\\DIM&\\AIM&\\CIM&\\crts\\\\ \n");
cat("\\hline\\hline\n");
#pdf(paste(c(paperDir,"testNum.pdf"),collapse=""), width=figWidth, height=figHeight)
t<-read.table("fse17-results/rts-testNum.dat", header=T)
avg<-numeric(14);
subCount<-0;
for(sub in subs){
    #if(sub=="commons-io"){
    #    break;
    #}
    subCount<-subCount+1;
    cat(sub);
    subdata<-subset(t,Sub==sub)
    methdata<-subset(subdata,Tech==0&Tag=="false")
    methmean<-mean(methdata[,c("TestNum")]);
    avg[1]<-avg[1]+methmean;
    cat(sep, print_decimal(methmean,2),"\\%",sep="");
    for(i in 1:14){
        if(i==13){next;}
        classdata<-subset(subdata,Tech==i&Tag=="false");
        classmean<-mean(classdata[,c("TestNum")]);
        diff<-classmean-methmean;
        if(diff==0){
            color<-"";
        }else{
            color<-"";#\\cellcolor{GrayOne}";
        }
        test<-"~\\equ";
        if(diff!=0){
            res<-wilcox.test(methdata[,c("TestNum")], classdata[,c("TestNum")], paired = TRUE)
            #print(res)
            #cat(mean(methdata[,c("TestNum")]));
            
            p<-res[["p.value"]];
            if(p<0.05){
                color<-"\\cellcolor{GrayTwo}";
                if(diff<0){
                    test<-"~\\decr";
                }else{
                    test<-"~\\incr"
                }
                if(FALSE){
                if(p<0.001)
                {test<-"***";}
                else if(p<0.01)
                {test<-"**";}
                else
                {test<-"*";}
                }
            }
        }
        cat(sep, color, paste(print_decimal(diff,2),"\\%",test,sep=""));
        if(i!=14){
            avg[i+1]<-avg[i+1]+diff;
        }else{
            avg[i]<-avg[i]+diff;
        }
        
    }
    if(sub=="commons-configuration"){
        cat("\\\\ \\hline\n");
    }else{
        cat("\\\\ \n");
    }
}
cat("\\hline\n");
cat("{\\bf Avg.}");
for(i in 1:14){
    cat(sep,paste(print_decimal(avg[i]/subCount,2),"\\%",sep=""));
}
cat("\\\\ \\hline\n");
cat("\\end{tabular}\n");
cat("\\end{adjustbox}\n");
cat("\\caption{\\label{tab:rts-testNum} Selection ratio change when transforming different atomic changes into file changes}\n");
cat("\\end{table*}\n");
sink();

