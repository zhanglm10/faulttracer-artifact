library(ggplot2)
library(reshape2)
library(wesanderson)

subs<-c("invokebinder", "compile-testing", "logstash-logback-encoder", "commons-cli", "joda-time", "commons-dbutils", "commons-validator", "commons-fileupload", "asterisk-java", "commons-functor", "la4j", "commons-jxpath", "commons-email", "commons-compress", "commons-codec", "jfreechart", "commons-collections", "commons-lang", "commons-imaging", "commons-configuration", "commons-net", "closure-compiler", "java-apns", "commons-io", "commons-math", "commons-dbcp", "log4j", "stream-lib", "HikariCP", "OpenTripPlanner", "commons-pool", "mapdb")

print_decimal <- function(x, k) format(round(x, k), nsmall=k)
sep<-"&"
print_cell<-function(p,v,c)
{if(p<0.05){
    if(mean(v)>mean(c)){
        cat(sep,pColor,print_decimal(mean(v),2),"\\%~\\incr",sep="");}
    else{
        cat(sep,pColor,print_decimal(mean(v),2),"\\%~\\decr",sep="");
    }
}else {
    cat(sep,print_decimal(mean(v),2),"\\%~\\equ",sep="");
}
}

print_cell2<-function(p,v,r,c)
{if(p<0.05){
    if(mean(v)>mean(c)){
        cat(sep,pColor,print_decimal(mean(v),2),"s~\\incr",sep,pColor, "(",print_decimal(mean(r),2),"\\%)",sep="");
    }else{cat(sep,pColor,print_decimal(mean(v),2),"s~\\decr",sep,pColor, "(",print_decimal(mean(r),2),"\\%)",sep="");}
    }
else{
    cat(sep,print_decimal(mean(v),2),"s~\\equ",sep, "(",print_decimal(mean(r),2),"\\%)",sep="");
}
}

compTechs<-c(0,15,13);
techNames<-c("\\hrts","\\hrtsb","\\hrtsn");
comp<-c("\\decr","\\equ","\\incr");
pColor<-"\\cellcolor{GrayTwo}";
#compTechs<-c(0,14,13);

sink("fse17-results/table7.tex");
cat("\\begin{table*}\n\\small\\center\n");
cat("\\begin{adjustbox}{width=1.0\\textwidth}");
cat("\\begin{tabular}{|l||rrr|rrrrrr|rrrrrr|}\n\\hline\n");
#cat("\\begin{tabular}{|l|l||lll|lll|lll|lll||l|}\n\\hline\n");
cat("&\\multicolumn{3}{c|}{Selected Tests}&\\multicolumn{6}{c|}{AE Time}&\\multicolumn{6}{c|}{AEC Time}\\\\ \\cline{2-16}\n");
cat("Subs");
for(i in 1:3){
    for(j in 1:3){
        if(i==1){cat("&",techNames[j]);}
        else{
            if(j==3){
                cat("&\\multicolumn{2}{c|}{",techNames[j],"}");
            }else{
                cat("&\\multicolumn{2}{c}{",techNames[j],"}");
            }
        }
    }
}
cat("\\\\ \n");
cat("\\hline\\hline\n");
#pdf(paste(c(paperDir,"testNum.pdf"),collapse=""), width=figWidth, height=figHeight)
mainTime<-read.table("fse17-results/rts-time.dat", header=T)
blockTime<-read.table("fse17-results/rts-time-block.dat", header=T)
mainNum<-read.table("fse17-results/rts-testNum.dat", header=T)
blockNum<-read.table("fse17-results/rts-testNum-block.dat", header=T)

avg<-numeric(9);
origTime<-0;
subCount<-0;
for(sub in subs){
    subCount<-subCount+1;
    cat(sub);
    subMainTime<-subset(mainTime,Sub==sub);
    subBlockTime<-subset(blockTime,Sub==sub);
    subMainNum<-subset(mainNum,Sub==sub);
    subBlockNum<-subset(blockNum,Sub==sub);
    
    
    cNumData<-subset(subMainNum,Tech==14&Tag=="false")[,c("TestNum")];
    tNumData<-subset(subMainNum,Tech==11&Tag=="false")[,c("TestNum")];
    mNumData<-subset(subMainNum,Tech==compTechs[1]&Tag=="false")[,c("TestNum")];
    bNumData<-subset(subBlockNum,Tech==compTechs[2]&Tag=="true")[,c("TestNum")];
    nNumData<-subset(subMainNum,Tech==compTechs[3]&Tag=="false")[,c("TestNum")];
    
    
    mNumP<-wilcox.test(cNumData, mNumData, paired = TRUE)[["p.value"]];
    bNumP<-wilcox.test(cNumData, bNumData, paired = TRUE)[["p.value"]];
    nNumP<-wilcox.test(cNumData, nNumData, paired = TRUE)[["p.value"]];
    
    
    avg[1]<-avg[1]+mean(mNumData);
    avg[2]<-avg[2]+mean(bNumData);
    avg[3]<-avg[3]+mean(nNumData);
    
    #cat(sep,print_decimal(mean(mNumData),2),"\\%",sep="");
    
    print_cell(mNumP,mNumData,cNumData);
    print_cell(bNumP,bNumData,cNumData);
    print_cell(nNumP,nNumData, cNumData);

    cAEData<-subset(subMainTime,Tech==14&Tag=="true")[,c("Time")];
    mAEData<-subset(subMainTime,Tech==compTechs[1]&Tag=="true")[,c("Time")];
    bAEData<-subset(subBlockTime,Tech==compTechs[2]&Tag=="true")[,c("Time")];
    nAEData<-subset(subMainTime,Tech==compTechs[3]&Tag=="true")[,c("Time")];

    cAERatio<-subset(subMainTime,Tech==14&Tag=="true")[,c("Ratio")];
    mAERatio<-subset(subMainTime,Tech==compTechs[1]&Tag=="true")[,c("Ratio")];
    bAERatio<-subset(subBlockTime,Tech==compTechs[2]&Tag=="true")[,c("Ratio")];
    nAERatio<-subset(subMainTime,Tech==compTechs[3]&Tag=="true")[,c("Ratio")];
    
    mAEP<-wilcox.test(cAEData, mAEData, paired = TRUE)[["p.value"]];
    bAEP<-wilcox.test(cAEData, bAEData, paired = TRUE)[["p.value"]];
    nAEP<-wilcox.test(cAEData, nAEData, paired = TRUE)[["p.value"]];
    
    origTime<-origTime+cAEData[1]/cAERatio[1];
    #cat(">>>>",cAEData[0],cAERatio[0],"\n");
    


    avg[4]<-avg[4]+mean(mAEData);
    avg[5]<-avg[5]+mean(bAEData);
    avg[6]<-avg[6]+mean(nAEData);

#cat(sep,print_decimal(mean(mAEData),2),"s",sep, "(",print_decimal(mean(mAERatio),2),"\\%)",sep="");
print_cell2(mAEP,mAEData,mAERatio,cAEData);
print_cell2(bAEP,bAEData,bAERatio,cAEData);
print_cell2(nAEP,nAEData,nAERatio,cAEData);
    

cAECData<-subset(subMainTime,Tech==14&Tag=="false")[,c("Time")];
mAECData<-subset(subMainTime,Tech==compTechs[1]&Tag=="false")[,c("Time")];
bAECData<-subset(subBlockTime,Tech==compTechs[2]&Tag=="false")[,c("Time")];
    nAECData<-subset(subMainTime,Tech==compTechs[3]&Tag=="false")[,c("Time")];
    
    cAECRatio<-subset(subMainTime,Tech==14&Tag=="false")[,c("Ratio")];
    mAECRatio<-subset(subMainTime,Tech==compTechs[1]&Tag=="false")[,c("Ratio")];
    bAECRatio<-subset(subBlockTime,Tech==compTechs[2]&Tag=="false")[,c("Ratio")];
    nAECRatio<-subset(subMainTime,Tech==compTechs[3]&Tag=="false")[,c("Ratio")];
    
    if(sub=="commons-codec"){
        bAECData<-bAEData;
        bAECRatio<-bAERatio;
    }

mAECP<-wilcox.test(cAECData, mAECData, paired = TRUE)[["p.value"]];
bAECP<-wilcox.test(cAECData, bAECData, paired = TRUE)[["p.value"]];
nAECP<-wilcox.test(cAECData, nAECData, paired = TRUE)[["p.value"]];

    avg[7]<-avg[7]+mean(mAECData);
    avg[8]<-avg[8]+mean(bAECData);
    avg[9]<-avg[9]+mean(nAECData);

#cat(sep,print_decimal(mean(mAECData),2),"s",sep, "(",print_decimal(mean(mAECRatio),2),"\\%)",sep="");
print_cell2(mAECP,mAECData,mAECRatio,cAECData);
print_cell2(bAECP,bAECData,bAECRatio,cAECData);
print_cell2(nAECP,nAECData,nAECRatio,cAECData);
if(sub=="commons-configuration"){
    cat("\\\\ \\hline\n");
}else{
cat("\\\\ \n");
}
}
cat("\\hline\n");
cat("{\\bf Avg.}");
for(i in 1:3){
    cat(sep,paste(print_decimal(avg[i]/subCount,2),"\\%",sep=""));
}
for(i in 4:9){
    cat(sep,paste(print_decimal(avg[i]/subCount,2),"s",sep, "(",print_decimal(avg[i]/origTime,2),"\\%)",sep=""));
}
cat("\\\\ \\hline\n");
cat("\\end{tabular}\n");
cat("\\end{adjustbox}\n");
cat("\\caption{\\label{tab:refineComp} Experimental results for \\hrts{} variants}\n");
cat("\\end{table*}\n");
sink();

