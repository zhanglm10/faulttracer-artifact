/**
 * The MIT License
 * Copyright © 2017 Lingming Zhang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.faulttracer.common;

import java.io.File;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.twdata.maven.mojoexecutor.MojoExecutor;

import set.faulttracer.utils.Properties;

//@Mojo(name = "test", defaultPhase = LifecyclePhase.TEST, requiresDependencyResolution = ResolutionScope.TEST)
public class AbstractCoverageMojo extends AbstractMojo
{
	/**
	 * Location of the binary directory.
	 */
	@Parameter(defaultValue = "${project.build.directory}", property = "outputDir", required = true)
	public File outputDirectory;

	/**
	 * The current maven project under test
	 */
	@Parameter(property = "project")
	protected MavenProject mavenProject;

	/**
	 * The dir of the maven project under test
	 */
	@Parameter(defaultValue = "${basedir}")
	protected File baseDir;

	/**
	 * Version of the current faulttracer
	 */
	@Parameter(defaultValue = "1.0-SNAPSHOT")
	protected String version;

	/**
	 * The level of coverage to collect
	 */
	@Parameter(defaultValue = "meth-cov")
	protected String coverageLevel;

	/**
	 * The path to the faulttracer jar
	 */
	@Parameter(defaultValue = "/Users/lingmingzhang/Documents/workspace/faulttracer/faulttracer-core/target/faulttracer-core-1.0-SNAPSHOT-jar-with-dependencies.jar")
	protected String jarLocation;

	/**
	 * The prefix for the packages under analysis. If none provided, coverage of
	 * all packages will be traced
	 */
	@Parameter(property = "prefix")
	protected String prefix;

	/**
	 * Whether to perform RTS or not
	 */
	@Parameter(property = "RTS", defaultValue = "true")
	protected boolean RTS;

	/**
	 * Test granularity
	 */
	@Parameter(property = "testMethLevel", defaultValue = "false")
	protected boolean testMethLevel;

	/**
	 * Whether to collect coverage or not
	 */
	@Parameter(property = "execOnly", defaultValue = "false")
	protected boolean execOnly;

	/**
	 * The hybrid level for faulttracer
	 */
	@Parameter(property = "hybridLevel")
	protected String hybridLevel;

	/**
	 * The location of the old version, default: the current dir
	 */
	@Parameter(property = "oldVersion", defaultValue = "${basedir}")
	protected String oldVersionLocation;

	/**
	 * The original argLine value for maven surefire
	 */
	private String originalArgLine;

	/**
	 * The faulttracer argLine value for maven surefire
	 */
	private String faulttracerArgLine;
	
	

	@Component
	protected MavenSession mavenSession;
	@Component
	protected BuildPluginManager pluginManager;

	protected Plugin surefire;

	public void execute() throws MojoExecutionException {
		getLog().info("outputDir: " + outputDirectory.getAbsolutePath());
		getLog().info("current project: " + mavenProject.toString());
		// getLog().info("properties: " +
		// mavenProject.getProperties().toString());
		getLog().info("baseDir: " + baseDir.getAbsolutePath());

		this.surefire = this
				.lookupPlugin("org.apache.maven.plugins:maven-surefire-plugin");
		if (this.surefire == null) {
			getLog().error("Make sure surefire is in your pom.xml!");
		}

		// execure maven surefire tests
		// mavenProject.getProperties().setProperty("argLine", "");
		Xpp3Dom domNode = (Xpp3Dom) this.surefire.getConfiguration();
		if (domNode == null) {
			domNode = new Xpp3Dom("configuration");
		}
		getLog().info("Original surefire config: \n" + domNode.toString());

		if (domNode.getChild("argLine") != null) {
			originalArgLine = domNode.getChild("argLine").getValue();
		} else {
			domNode.addChild(new Xpp3Dom("argLine"));
		}

		setFaultTracerArgLine();
		domNode.getChild("argLine").setValue(this.faulttracerArgLine);

		getLog().info(this.mavenProject + "\n" + this.mavenSession + "\n"
				+ this.pluginManager);
		getLog().info("Modified surefire config: \n" + domNode.toString());

		MojoExecutor.executeMojo(this.surefire, "test", domNode,
				MojoExecutor.executionEnvironment(this.mavenProject,
						this.mavenSession, this.pluginManager));
	}

	private void setFaultTracerArgLine() {
		getFaultTracerVersion();
		String testLevel = testMethLevel ? Properties.TM_LEVEL
				: Properties.TC_LEVEL;
		String localRepo = this.mavenSession.getSettings().getLocalRepository();
		this.jarLocation = getPathToFaultTracerJar(localRepo);
		faulttracerArgLine = "-javaagent:" + this.jarLocation + "="
				+ (this.prefix == null ? ""
						: Properties.PREFIX_KEY + this.prefix + ",")
				+ Properties.COV_TYPE_KEY + this.coverageLevel + ","
				+ Properties.RTS_KEY + this.RTS + ","
				+ (this.hybridLevel == null ? ""
						: Properties.LEVEL_KEY + this.hybridLevel + ",")
				+ Properties.TEST_LEVEL_KEY + testLevel + ","
				+ Properties.NEW_CLASSPATH_KEY + this.outputDirectory + ","
				+ Properties.EXECUTION_ONLY_KEY + this.execOnly + ","
				+ Properties.NEW_DIR_KEY + this.baseDir + ","
				+ Properties.OLD_DIR_KEY + this.oldVersionLocation;
		/*if (this.originalArgLine != null)
			faulttracerArgLine = faulttracerArgLine + " "
					+ this.originalArgLine;*/
	}

	private Plugin lookupPlugin(String paramString) {
		List<Plugin> localList = this.mavenProject.getBuildPlugins();
		Iterator<Plugin> localIterator = localList.iterator();
		while (localIterator.hasNext()) {
			Plugin localPlugin = localIterator.next();
			if (paramString.equalsIgnoreCase(localPlugin.getKey())) {
				return localPlugin;
			}
		}
		return null;
	}

	private String getPathToFaultTracerJar(String localRepo) {
		String result = Paths
				.get(mavenSession.getLocalRepository().getBasedir(), "set",
						"faulttracer", "faulttracer-core", version,
						"faulttracer-core-" + version + ".jar")
				.toString();
		return result;
	}

	private void getFaultTracerVersion() {
		Artifact artifact = (Artifact) this.mavenProject.getPluginArtifactMap()
				.get("set.faulttracer:faulttracer-maven-plugin");
		this.version = artifact.getVersion();
	}
}
